import logging
import os
from datetime import date
import jinja2
import s3_handler
import pdfkit
template_bucket = os.environ["TEMPLATE_BUCKET"]
template_file_key = os.environ["TEMPLATE_FILE_KEY"]
payroll_bucket = os.environ["PAYROLL_BUCKET"]


def fetch_template_from_s3():
    template_file = s3_handler.download_s3_file(template_bucket, template_file_key)
    return template_file

def upload_payroll_to_s3(payroll_pdf):
    uploaded_file_key = s3_handler.upload_file_to_s3(payroll_bucket,payroll_pdf)
    return uploaded_file_key
def generate_payroll(employee):
    """
    Render html page using jinja
    """
    template_file = fetch_template_from_s3()
    template_loader = jinja2.FileSystemLoader(searchpath='/tmp/')
    template_env = jinja2.Environment(loader=template_loader)
    template = template_env.get_template(template_file)
    output_text = template.render(
        name=employee.name,
        surName=employee.surName,
        address=employee.address,
        employeeNumber=employee.employeeNumber,
        department=employee.department["name"],
        role=employee.role,
        date=get_date(),
        grossSalary=employee.grossSalaryYearly,
        taxClass=employee.taxClass,
        netSalary=employee.netSalary
    )

    html_path = f'/tmp/{employee.name + employee.surName}_{get_date()}.html'
    html_file = open(html_path, 'w')
    html_file.write(output_text)
    html_file.close()
    logging.info(f"Now converting html to {employee.name + employee.surName}_{get_date()}.pdf ... ")
    pdf_path = f'/tmp/{employee.name + employee.surName}_{get_date()}.pdf'
    html2pdf(html_path, pdf_path)
    logging.info(f"Payroll PDF has been created for {employee.name} {employee.surName}")
    logging.info(f"Uploading payroll to S3...")
    uploaded_file = upload_payroll_to_s3(pdf_path)

def html2pdf(html_path, pdf_path):
    """
    Convert html to pdf using pdfkit which is a wrapper of wkhtmltopdf
    """
    try:
        PATH_WKHTMLTOPDF = '/opt/bin/wkhtmltopdf'
        PDFKIT_CONFIG = pdfkit.configuration(wkhtmltopdf=PATH_WKHTMLTOPDF)
        options = {
            'page-size': 'Letter',
            'margin-top': '0.35in',
            'margin-right': '0.75in',
            'margin-bottom': '0.75in',
            'margin-left': '0.75in',
            'encoding': "UTF-8",
            'no-outline': None,
            'enable-local-file-access': None
        }
        with open(html_path) as f:
            pdfkit.from_file(f, pdf_path, options=options, configuration=PDFKIT_CONFIG)
    except  OSError as error:
        logging.error(error)



def get_date():
    today = date.today()
    return today.strftime("%d.%m.%Y")
