class Employee:
    def __init__(self, id, name, surName, department, employeeNumber, role, address, taxClass, grossSalaryYearly, netSalary):
        self.id = id
        self.name = name
        self.surName = surName
        self.department = department
        self.employeeNumber = employeeNumber
        self.role = role
        self.address = address
        self.taxClass = taxClass
        self.grossSalaryYearly = grossSalaryYearly
        self.netSalary = netSalary
