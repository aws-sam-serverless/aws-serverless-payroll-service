import logging
from typing import Optional
import boto3
import botocore.exceptions

logger = logging.getLogger()
logger.setLevel(logging.INFO)

# Get the s3 client
#s3 = boto3.client('s3',
#        endpoint_url=os.environ["AWS_URL"],
#        use_ssl=False)
s3 = boto3.client('s3')

def download_s3_file(bucket: str, file_key: str) -> str:
    """Downloads a file from s3 to `/tmp/[File Key]`.

    Args:
        bucket (str): Name of the bucket where the file lives.
        file_key (str): The file key of the file in the bucket.
    Returns:
    	The local file name as a string.
    """
    local_filename = f'/tmp/{file_key}'
    s3.download_file(Bucket=bucket, Key=file_key, Filename=local_filename)
    logger.info('Downloaded HTML file to %s' % local_filename)

    return file_key


def upload_file_to_s3(bucket: str, filename: str) -> Optional[str]:
    """Uploads the generated PDF to s3.

    Args:
        bucket (str): Name of the s3 bucket to upload the PDF to.
        filename (str): Location of the file to upload to s3.

    Returns:
        The file key of the file in s3 if the upload was successful.
        If the upload failed, then `None` will be returned.
    """
    file_key = None
    try:
        file_key = filename.replace('/tmp/', '')
        s3.upload_file(Filename=filename, Bucket=bucket, Key=file_key)
        logger.info('Successfully uploaded the PDF to %s as %s'
                    % (bucket, file_key))
    except botocore.exceptions.ClientError as e:
        logger.error('Failed to upload file to s3.')
        logger.error(e)
        file_key = None

    return file_key
