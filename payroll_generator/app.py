import json
import payroll_generator
import logging
from employee import Employee

logger = logging.getLogger()
logger.setLevel(logging.INFO)
def lambda_handler(event, context):
    for record in event['Records']:
        employee = json.loads(record["body"])
        employee = Employee(**employee)
        logger.info(employee)
        logger.info('Connecting S3 bucket....')
        payroll_generator.generate_payroll(employee)

